#-*- coding: utf-8 -*-

# Copyright 2010 Calculate Ltd. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0 #
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

#Допустимые ключи значений
#   mode -     read only or writeable variable
#   value    - default variable value
#   select   - list of posible values for variable
#   hide - flag, if it is True, then the variable is not printable
#   printval - print value of variable
from cl_install import __version__, __app__

class Data:
    # relative path for apply templates on files of system
    cl_root_path = {}

    # program name
    cl_name = {'value':__app__}

    # program version
    cl_ver = {'value':__version__}

    """
    Action variable which has value "up" by default and not flash installation
    """
    ac_install_merge = {}

    """
    Action variable which has value "up" for installation on hdd
    """
    ac_install_system = {}

    """
    Action variable which has value "up" for USB flash
    """
    ac_install_flash = {}

    """
    Action variable which has value "up" for PXE installation
    """
    ac_install_pxe = {}

    """
    Action which "up" value describe dinamic templates
    """
    ac_install_live = {}

    # install maching architecture
    os_install_arch_machine = {'mode':'w'}

    # inforamation about net interfaces
    os_net_interfaces_info = {}

    # infomation about disk in hash
    os_device_hash = {'hide':True}

    # infomation about disk in hash
    os_disk_hash = {'hide':True}

    # list of available partition devices
    os_disk_dev = {}

    # list mounted points for installed system
    os_install_disk_mount = {}

    # list mounted points for current operation system
    os_disk_mount = {}

    # partition content
    os_disk_content = {}

    # list filesystem for partition devices
    os_disk_format = {}

    # list type (lvm,raid,partition,disk)
    os_disk_type = {}

    # install list filesystem for partition devices
    os_install_disk_format = {}

    # need format
    os_install_disk_perform_format = {}

    # uudi for install
    os_install_disk_uuid = {}

    # /dev/sd or UUID= list (by cl_uuid_set)
    os_install_disk_use = {}

    cl_uuid_set = {'value':'on'}

    # install partition's system id
    os_install_disk_id = {}

    # list uudi for partition devices
    os_disk_uuid = {}

    # partition's system id
    os_disk_id = {}

    # disk for boot mbr
    os_install_mbr = {}

    # list grub id for partition devices
    os_disk_grub = {}

    # install list grub id for partition devices (depend from mbr)
    os_install_disk_grub = {}

    # type of partition devices (primary, extended or logical)
    os_disk_part = {}

    # partition table on parent device 
    os_disk_table = {}

    # partition size
    os_disk_size = {}

    # label of partitions
    os_disk_name = {}

    # list mount options
    os_disk_options = {}

    # devices
    os_device_dev = {}

    # device type (hdd,cdrom,usb-flash)
    os_device_type = {}

    # map number for grub
    os_device_map = {}

    # table for device
    os_device_table = {}

    # content of device.map file for grub
    os_install_grub_devicemap_conf = {}

    # information about mount points for fstab
    os_install_fstab_mount_conf = {}

    # information about swap for fstab
    os_install_fstab_swap_conf = {}

    # content of /etc/conf.d/net
    #os_net_config_info = {}

    # install scheduler
    os_install_kernel_scheduler = {}

    # list of schedulers
    os_kernel_schedulers = {}

    # install kernel attributes
    os_install_kernel_attr = {}

    # install kernel resume
    os_install_kernel_resume = {}

    # install cpufreq modules
    os_install_kernel_cpufreq = {}

    # system image for installation
    cl_image = {}

    # path which contains images
    cl_image_path = {}

    # DISTFILES value
    cl_distfiles_path = {'mode':'w',
                         'value':'/var/calculate/remote/distfiles'}
    # PKGDIR value
    cl_pkgdir_path = {'mode':'w'}

    # LINGUAS value
    os_install_linguas = {'mode':'w'}

    # linux version of installation os
    os_install_linux_ver = {'mode':'r'}

    # linux build of installation os
    os_install_linux_build = {'mode':'r'}

    # subname of installation os
    os_install_linux_subname = {'mode':'w'}

    # shortname of installation os
    os_install_linux_shortname = {'mode':'w'}

    # name of installation os
    os_install_linux_name = {'mode':'w'}

    # installation os  system: server or desktop
    os_install_linux_system = {}

    # root device of installed os
    os_install_root_dev = {}

    # root device of previous installed os
    os_install_dev_from = {}
    
    # list mount options of installed os
    os_install_disk_options = {}

    #
    os_bind_hash = {'hide':True}

    # directories for bind
    os_bind_path = {}

    # mountpoint for directories bind
    os_bind_mountpoint = {}

    os_install_bind_hash = {'hide':True}

    # install directories for bind
    os_install_bind_path = {}

    # mountpoint for install directories bind
    os_install_bind_mountpoint = {}

    # busid of video card
    hr_video_id = {'value':""}

    # timezone for clock
    os_install_clock_timezone = {'mode':'w'}

    # type of clock (UTC or local)
    os_install_clock_type = {'mode':'w'}

    # xorg resolution
    os_install_x11_resolution = {}

    # fb resolution
    os_install_fb_resolution = {'mode':'w'}

    # makeconf makeopts
    os_install_makeopts = {'mode':'w'}

    # Video driver used by xorg
    os_install_x11_video_drv = {'mode':'w'}

    # on/off composite
    os_install_x11_composite = {'mode':'w'}

    # consolefont
    os_install_locale_consolefont = {}

    # keymap of locale (used for /etc/conf.d/keymaps)
    os_install_locale_keymap = {}

    # dumpkeys_charset for keymap
    os_install_locale_dumpkeys = {}

    # locale (at example: ru_RU.UTF-8)
    os_install_locale_locale = {}

    # full language (at example: ru_RU)
    os_install_locale_lang = {}

    # short language (at example ru)
    os_install_locale_language = {}

    # keyboard layout for X server
    os_install_locale_xkb = {}

    # keyboard layout name for X server
    os_install_locale_xkbname = {}

    # hash for information about net
    os_install_net_hash = {}

    # computer hostname
    os_install_net_hostname = {'mode':"w"}

    # allowed network
    os_install_net_allow ={}

    # net interfaces
    os_install_net_interfaces={}

    # net device name
    os_install_net_name={}

    # net devices mac
    os_install_net_mac={}

    # ip for all network interfaces
    os_install_net_ip = {}

    # network for ip
    os_install_net_network = {}

    # ip cidr
    os_install_net_cidr = {}

    # routing
    os_install_net_route_hash = {}

    # net for route
    os_install_net_route_network = {}

    # gw for route
    os_install_net_route_gw = {}

    # dev for route
    os_install_net_route_dev = {}

    # src for route
    os_install_net_route_src = {}

    # data by route for conf.d/net
    os_install_net_route = {'hide':True}

    # data by route for NetworkManager
    os_install_net_nmroute = {'hide':True}

    # dns servers
    os_install_net_dns = {}

    # net setup (networkmanager or openrc)
    os_install_net_conf = {}

    # net service configured
    os_install_net_settings = {'mode':'w',
                               'value':''}

    # dhcp or not
    os_install_net_dhcp_set = {}

    # dns search
    os_install_net_dns_search = {'mode':"w"}

    # domain
    os_install_net_domain = {'mode':"w"}

    # type of device for install
    os_install_root_type = {'mode':'w'}

    # using lvm
    os_install_lvm_set = {}

    # using mdadm
    os_install_mdadm_set = {}

    # proxy server for system
    os_install_proxy = {'mode':'w',
                        'value':''}

    # nt server for system
    os_install_ntp = {'mode':'w',
                      'value':'ntp0.zenon.net'}

    # kernel filename
    os_install_kernel = {}

    # optimized initramfs filename
    os_install_initrd = {}

    # install initramfs filename
    os_install_initrd_install = {}

    # install config kernel filename
    os_install_kernel_config = {}

    # install system map filename
    os_install_system_map = {}

    # install kernel uid
    cl_install_kernel_uid = {}

    # filesystem format support by calcualte-install
    os_format_type = {}

    # avialable format by mkfs utility
    os_format_use = {}

    # current grub
    os_grub_conf = {}

    # grub2 install path
    os_grub2_path = {'mode':'w'}

    # grub chroot need for grub-mkconfig
    cl_chroot_grub = {}

    # migrate users
    cl_migrate_user = {}

    # migrate users who need to change passwords
    cl_migrate_user_pwd = {}

    # install system in scratch mode
    os_install_scratch = {}

    # install system for pxe boot
    os_install_pxe = {'value':'off'}

    # path to pxe installation
    os_install_pxe_path = {'value':'/var/calculate/pxe'}

    # nvidia-drivers atom mask
    os_nvidia_mask = {}

    # (on or off) autoupdate config from install program
    cl_autoupdate_set = {'value': "off"}

    # (on or off) autoupdate config from install program for install
    cl_install_autoupdate_set = {'mode':'w','value': "off"}

    # variable for autologin
    cl_autologin = {}

    # lib vars
    os_locale_xkb = {}
    hr_video = {}
    hr_video_name = {}
    os_linux_name = {}
    os_scratch = {}
    os_x11_video_drv = {}
    hr_cpu_num = {}
    os_locale_locale = {}
    os_net_interfaces = {}
    cl_template_clt_path = {}
    os_linux_ver = {}
    os_net_allow = {}
    cl_kernel_uid = {}
    os_arch_machine = {}
    cl_template_path = {}
    os_net_hostname = {'mode':"w"}
    cl_chroot_path = {'mode':"w"}
    cl_env_path = {}
    os_root_dev = {}
    os_linux_shortname = {}
    os_net_ip = {}
    os_root_type = {}
    hr_laptop = {}
    hr_laptop_model = {}
    os_locale_language = {}
    cl_root_path = {'mode':"w"}
