#-*- coding: utf-8 -*-

# Copyright 2010 Calculate Ltd. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import os
import sys
import re
from crypt import crypt
import cl_overriding
from cl_datavars import glob_attr
from os import path
from os import readlink,access,R_OK
from cl_utils import isMount,typeFile,getTupleVersion,pathJoin,isFstabMount,\
                     listDirectory, getAvailableVideo, getUUIDDict, \
                     getUdevDeviceInfo, getPartitionDevice, getPartitionSize, \
                     isPkgInstalled, process, checkUtils, readLinesFile, \
                     FStab, lspci, getInstalledVideo,getDeviceType, \
                     getPartitionType, getOsProberHash, getProgPath, \
                     getRaidPartitions, getLvmPartitions, getLvmGroups, \
                     getPasswdUsers, readFile, readLinesFile
from cl_distr import DistributiveRepository,PartitionDistributive
from cl_fill import clLocale
from operator import itemgetter
from cl_template import _terms
from subprocess import PIPE, Popen
from cl_install import convertDictOpt
from utils import ip

from cl_lang import lang
tr = lang()
tr.setLocalDomain('cl_install')
tr.setLanguage(sys.modules[__name__])

class fillVars(object, glob_attr):
    nonTransferedDirs = ["/","/bin", "/dev", "/etc",
                           "/lib", "/lib32", "/lib64",
                           "/opt", "/proc", "/sbin",
                           "/sys", "/usr", "/var"]

    reWrongDevice = re.compile("|".join(['^fd','^ram','^loop']))

    def removeFloppy(self):
        """Remove floopy device and return floppyData"""
        floppyPath = '/dev/fd1'
        if path.exists(floppyPath):
            info = os.lstat(floppyPath)
            os.unlink(floppyPath)
            return (floppyPath,info)
        return None

    def restoreFloppy(self,floppyData):
        """Restore floppy device by floppyData"""
        try:
            if floppyData:
                os.mknod(floppyData[0],floppyData[1].st_mode,
                         floppyData[1].st_rdev)
                os.chown(floppyData[0],floppyData[1].st_uid,
                                       floppyData[1].st_gid)
        except:
            pass

    def get_ac_install_merge(self):
        """Need perform templates for install:merge or install:unmerge"""
        actionsMap = {'merge':'up',
                      'system':'up'}
        cl_action = self.Get('cl_action')
        if cl_action == 'system' and (
            self.Get('os_install_root_type') == 'flash' or
            self.Get('os_install_pxe') == 'on'):
            return ""
        return actionsMap[cl_action] if cl_action in actionsMap else ""

    def get_ac_install_system(self):
        """Need perform templates for install:system"""
        if self.Get('cl_action') == 'system' and \
            self.Get('os_install_root_type') != "flash" and \
            self.Get('os_install_pxe') != "on":
            return "up"
        else:
            return ""

    def get_ac_install_flash(self):
        """Need perform templates for install:flash"""
        clAction = self.Get('cl_action')
        if clAction == 'system' and self.Get('os_install_root_type') == 'flash':
            return "up"
        return ""

    def get_ac_install_pxe(self):
        """Need perform templates for install:flash"""
        clAction = self.Get('cl_action')
        if clAction == 'system' and self.Get('os_install_pxe') == 'on':
            return "up"
        return ""

    def get_ac_install_live(self):
        """Need perform templates for install:live"""
        if self.Get('ac_install_merge') == "up" and \
            self.Get('cl_chroot_status') == "off"  or \
            self.Get('cl_action') == 'live':
            return "up"
        else:
            return ""

    def get_os_install_net_hash(self):
        """Get net hash"""
        rePci = re.compile(r"(\d\d:\d\d\.\d)(?:/[^/]+){2}$")
        def getPci(interface):
            pathname = path.realpath(path.join('/sys/class/net',
                                                       interface))
            pci = rePci.search(pathname)
            if pci:
                return pci.group(1)
            else:
                return ""
        interfaces = ip.getInterfaces()
        net_hash = {}
        pciEthernet = lspci(shortInfo=True)
        nfsRoot = self.Get('os_install_root_dev') == '/dev/nfs'
        for interface in interfaces:
            ipaddr,mask = ip.receiveIpAndMask(interface)
            dhcp = ip.isDhcpIp(interface)
            mac = ip.receiveMac(interface)
            pciInfo = pciEthernet.get(getPci(interface),
                                      {'vendor':_("Unknown"),
                                       'name':_("vendor")})
            mapInterface = {}
            mapInterface["ip"] = ipaddr
            mapInterface["cidr"] = str(ip.maskToCidr(mask)) if mask else ""
            if ipaddr and mask:
                mapInterface["network"] = ip.getIpNet(ipaddr,mask=mask)
            else:
                mapInterface["network"] = ""
            mapInterface["dhcp"] = "on" if dhcp or nfsRoot else "off"
            mapInterface["mac"] = mac
            mapInterface["name"] = "{vendor} {name}".format(**pciInfo)
            net_hash[interface]=mapInterface
        return net_hash

    def get_os_install_net_dhcp_set(self):
        """DHCP or not"""
        return self.getAttributeFromHash('os_install_net_hash','dhcp')

    def get_os_install_net_route_hash(self):
        """Routing hash"""
        interfaces = self.Get('os_install_net_interfaces')
        interfaces_dhcp = self.Get('os_install_net_dhcp_set')
        staticInterface = \
            map(lambda x:x[0],
            filter(lambda x:x[1] == "off",
            zip(interfaces,interfaces_dhcp)))
        route_hash = {}
        if staticInterface:
            for network,routeParam in ip.getRouteTable(staticInterface):
                param = {}
                param['gw'] = routeParam.get('via','')
                param['src'] = routeParam.get('src','')
                param['dev'] = routeParam.get('dev','')
                route_hash[network] = param
        return route_hash

    def performRouteData(self,performFunc):
        routeMatrix = zip(self.Get('os_install_net_route_network'),
                          self.Get('os_install_net_route_gw'),
                          self.Get('os_install_net_route_dev'),
                          self.Get('os_install_net_route_src'))
        DEV,IP,CIDR,NET = 0,1,2,1
        return map(lambda x:performFunc(x[DEV],x[NET],routeMatrix),
               # union ip and mask to ip/net
               map(lambda x:(x[DEV],ip.getIpNet(x[IP],cidr=x[CIDR])),
               filter(lambda x:x[IP] and x[CIDR],
               zip(self.Get('os_install_net_interfaces'),
                   self.Get('os_install_net_ip'),
                   self.Get('os_install_net_cidr')))))

    def get_os_install_net_nmroute(self):
        """Route info for system-connections of NetworkManager"""
        def getRouteForInterfaceNM(interface,net,routeMatrix):
            NET,GW,DEV,SRC = 0,1,2,3
            defaultGw = map(lambda x:"%s;"%x[GW],
                        filter(lambda x:interface==x[DEV] and \
                                         x[NET]=="default",
                        routeMatrix))
            return "{0}\n".format(defaultGw[0] if defaultGw else "") + \
               "\n".join(
               # build string for route from net,gateway,dev and src
               map(lambda x:"routes{num}={ip};{cidr};{gateway};0;".format(
                   num=x[0]+1,
                   ip=x[1][NET].partition('/')[0],
                   cidr=x[1][NET].partition('/')[2],
                   gateway=x[1][GW] if x[1][GW] else "0.0.0.0"),
               # filter by interface and discard direct routes
               # example: for 192.168.1.5/24 discard 192.168.1.0/24 net
               enumerate(
               filter(lambda x:interface==x[DEV] and net!=x[NET] and \
                               x[NET]!="default",routeMatrix))))
        return self.performRouteData(getRouteForInterfaceNM)

    def get_os_install_net_route(self):
        """Route info for conf.d/net"""
        def getRouteForInterfaceConf(interface,net,routeMatrix):
            NET,GW,DEV,SRC = 0,1,2,3
            return "\n".join(
               # build string for route from net,gateway,dev and src
               map(lambda x:"{net}{gateway}{src}".format(
                   net=x[NET],
                   gateway=" via %s"%x[GW] if x[GW] else "",
                   src=" src %s"%x[SRC] if x[SRC] else ""),
               # filter by interface and discard direct routes
               # example: for 192.168.1.5/24 discard 192.168.1.0/24 net
               filter(lambda x:interface==x[DEV] and net!=x[NET],routeMatrix)))
        return self.performRouteData(getRouteForInterfaceConf)
        

    def get_os_install_net_route_network(self):
        return sorted(self.Get('os_install_net_route_hash').keys())

    def get_os_install_net_route_gw(self):
        return self.getAttributeFromHash('os_install_net_route_hash','gw')

    def get_os_install_net_route_dev(self):
        return self.getAttributeFromHash('os_install_net_route_hash','dev')

    def get_os_install_net_route_src(self):
        return self.getAttributeFromHash('os_install_net_route_hash','src')

    def get_os_net_interfaces_info(self):
        """Информация о существующих сетевых интерфейсах"""
        netInterfaces=self.Get("os_net_interfaces")
        listInterfacesInfo = []
        # Получена ли сеть по DHCP если нет to ip или off
        for interface,ipaddr,dhcp in zip(self.Get('os_install_net_interfaces'),
                                     self.Get('os_install_net_ip'),
                                     self.Get('os_install_net_dhcp_set')):
            if dhcp == "on":
                listInterfacesInfo.append((interface, "DHCP"))
            else:
                listInterfacesInfo.append((interface,
                                           ipaddr if ipaddr else "Off"))
        return ", ".join(map(lambda x:"%s (%s)"%(x[0],x[1]),
                             listInterfacesInfo))

    
    def getTableByChild(self,device):
        """Get table by child partitions"""
        syspath = getUdevDeviceInfo(name=device).get('DEVPATH','')
        if not syspath.startswith('/sys'):
            syspath = pathJoin('/sys',syspath)
        shortnameDevice = path.basename(device)
        childs = filter(lambda x:x.startswith(shortnameDevice),
                 listDirectory(syspath))
        if childs:
            child = pathJoin(syspath,childs[0])
            return getUdevDeviceInfo(path=child).get('ID_PART_ENTRY_SCHEME','')
        return ""
                 
    def get_os_device_hash(self):
        """Generate hash information about device"""
        def onlyDisk(devpath):
            """Check by udevadm that devpath is device (disk)"""
            prop = getUdevDeviceInfo(devpath)
            return prop.get("ID_TYPE","")=="disk" and \
                   prop.get("DEVTYPE","")=="disk"

        # get usb device by '/dev/disk/by-id'(usb devices contain 'usb' in name)
        diskIdPath = '/dev/disk/by-id'
        if path.exists(diskIdPath):
            usbdevices = \
             map(lambda x: readlink(path.join(diskIdPath,x)).rpartition('/')[2],
             filter(lambda x: x.startswith('usb-'),listDirectory(diskIdPath)))
        else:
            usbdevices = []
        # get devices from /sys/block directories(discard mem,sr,loop and other)
        sysBlockPath = '/sys/block'
        devices = map(lambda x:path.join(sysBlockPath,x),
                  filter(lambda x: onlyDisk(path.join(sysBlockPath,x)),
                  filter(lambda x: not self.reWrongDevice.search(x),
                  listDirectory(sysBlockPath))))
        device_hash = {}
        # filling hash
        for mapnum,device in enumerate(sorted(devices,key=self.separateDevice)):
            # get information by udev
            props = getUdevDeviceInfo(device)
            if not "DEVNAME" in props:
                continue
            # DEVNAME - /dev/(device_name)
            device = props['DEVNAME']
            device_hash[device] = {}
            # get partition table
            # (if PART_TABLE_TYPE absent then get by child partition)
            device_hash[device]['table'] = props.get('ID_PART_TABLE_TYPE',
                                           self.getTableByChild(device))
            # enumerate disk for legecy grub
            device_hash[device]['map'] = mapnum
            # if device is usb device
            if path.basename(device) in usbdevices:
                # check for usb flash (removeable fiel in sysfs contains "1")
                removablePath = '/sys/block/%s/removable'%path.basename(device)
                if os.access(removablePath,R_OK) and \
                    open(removablePath,'r').read().strip() == "1":
                    devtype = "flash" 
                else:
                    devtype = "usb-hdd" 
            else:
                devtype = "hdd" 
            # set detect device type (hdd,flash or usb-hdd)
            device_hash[device]['type'] = devtype
        return device_hash

    def get_os_disk_hash(self):
        """Get information about disk, which use for installation"""
        def selfOrPartition(devpath):
            """Return self device or partitions if it has them"""
            # search partition in device
            # get devices in sysfs which startswith devname
            partitions = \
                filter(lambda x:x.startswith(path.basename(devpath)),
                listDirectory(devpath))
            # if partition found then return them or partition on them
            if partitions:
                return filter(lambda x:x,
                       map(lambda x:selfOrPartition(path.join(devpath,x)),
                       partitions))
            else:
                return devpath

        devicesHash = self.Get('os_device_hash')
        sysBlockPath = '/sys/block'
        # get disks from sys block which not contains partitions
        new_disks = reduce(lambda x,y:x+ ([y] if type(y) == str else y),
                    map(lambda x:selfOrPartition(path.join(sysBlockPath,x)),
                    filter(lambda x: not self.reWrongDevice.search(x),
                    listDirectory(sysBlockPath))),[])
        disk_hash = {}

        fstab = FStab('/etc/fstab')
        raidUsedDisks = []
        lvmUsedDisks = []
        mapTypeUUID = {'ebd0a0a2-b9e5-4433-87c0-68b6b72699c7':'0700',
                       '0657fd6d-a4ab-43c4-84e5-0933c84b4f4f':'8200',
                       'a19d880f-05fc-4d3b-a006-743f0f84911e':'FD00',
                       '21686148-6449-6e6f-744e-656564454649':'EF02'}
        # filling information about disks
        for disk in new_disks:
            props = getUdevDeviceInfo(disk)
            if not "DEVNAME" in props:
                continue
            devName = props['DEVNAME']
            dev_hash = {}
            dev_hash['uuid'] = props.get('ID_FS_UUID','')
            dev_hash['format'] = props.get('FSTAB_TYPE') or \
                                 fstab.getBy(what=fstab.TYPE,eq=devName) or \
                                 props.get('ID_FS_TYPE','')
            dev_hash['name'] = props.get('ID_FS_LABEL','')
            dev_hash['table'] = props.get('ID_PART_ENTRY_SCHEME','')
            # schema device (disk-partition or disk-partition-raid and etc)
            dev_hash['type'] = getDeviceType(disk)
            dev_hash['id'] = \
                props.get('ID_PART_ENTRY_TYPE','').rpartition("x")[2]
            dev_hash['id'] = mapTypeUUID.get(dev_hash['id'],dev_hash['id'])
            devParent = getPartitionDevice(disk)
            dev_hash['parent'] = devParent
            # primary, extended or logical
            dev_hash['part'] = getPartitionType(props)
            # get options from fstab
            dev_hash['options'] = fstab.getBy(what=fstab.OPTS,eq=devName) or ""
            # get disk size
            dev_hash['size'] = getPartitionSize(disk)
            if devParent in devicesHash and 'ID_PART_ENTRY_NUMBER' in props:
                dev_hash['grub'] = "%s,%d"%(devicesHash[devParent]['map'],
                                    int(props['ID_PART_ENTRY_NUMBER'])-1)
            else:
                dev_hash['grub'] = ""
            disk_hash[devName] = dev_hash
            # if device raid then get its partition and
            # add to raid partitions list
            if "raid" in dev_hash['type']:
                raidUsedDisks.extend(map(lambda x:(devName,x),
                                         getRaidPartitions(disk)))
            # if device lvm then get its partition and 
            # add to lvm partitions list
            if dev_hash['type'].endswith("lvm"):
                prop = getUdevDeviceInfo(path=disk)
                lvmUsedDisks.extend(map(lambda x:(devName,x),
                                getLvmPartitions(prop.get('DM_VG_NAME',''),
                                                 prop.get('DM_LV_NAME',''))))
        # fix type information for raid disks
        for disk,part in raidUsedDisks:
            if part in disk_hash:
                disk_hash[part]['type'] = "%s-raidmember(%s)"%(
                                          disk_hash[part]['type'],disk)
        # fix type information for lvm disks
        for disk,part in lvmUsedDisks:
            if part in disk_hash:
                disk_hash[part]['type'] = "%s-lvmmember(%s)"%(
                                          disk_hash[part]['type'],disk)
        return disk_hash

    def separateDevice(self,device):
        return map(lambda x: int(x) if x.isdigit() else x,
                   re.findall('\d+|\D+',device))

    def get_os_disk_dev(self):
        """List of available partition devices"""
        return sorted(self.Get('os_disk_hash').keys(),
                      key=self.separateDevice)

    def getAttributeFromHash(self,var,attr):
        hash = self.Get(var)
        return map(lambda x: hash[x][attr] if attr in hash[x] else "",
                sorted(hash.keys(),
                       key=self.separateDevice))

    def get_os_install_disk_id(self):
        """List id for partition after installation"""
        diskHash = self.Get('os_disk_hash')
        def getIdByFS(fs,parttable,oldid):
            if parttable == "dos":
                return PartitionDistributive.formatId.get(fs,oldid)
            elif parttable == "gpt":
                return PartitionDistributive.formatIdGpt.get(fs,oldid)
            return oldid
        return map(lambda x:getIdByFS(x[0],
                        diskHash.get(x[2],{}).get('table',None),x[1]),
                   zip(self.Get('os_install_disk_format'),
                       self.Get('os_disk_id'),
                       self.Get('os_disk_dev')))

    def get_os_disk_id(self):
        """List id for partition devices"""
        return self.getAttributeFromHash('os_disk_hash','id')

    def get_os_disk_uuid(self):
        """List uuid for partition devices"""
        return self.getAttributeFromHash('os_disk_hash','uuid')

    def get_os_disk_options(self):
        """List mount options for partition devices"""
        return self.getAttributeFromHash('os_disk_hash','options')

    def get_os_install_disk_use(self):
        """Get real id (by cl_uuid_set) device"""
        if self.Get('cl_uuid_set') == "on":
            return map(lambda x:"UUID=%s"%x[0] if x[0] else x[1],
                   zip(self.Get('os_install_disk_uuid'),
                       self.Get('os_disk_dev')))
        else:
            return self.Get('os_disk_dev')

    def get_os_install_disk_uuid(self):
        """List uuid for partition devices"""
        diskDev = self.Get('os_disk_dev')
        hashUUID = getUUIDDict(revers=True)
        return map(lambda x:hashUUID.get(x,"")[5:],diskDev)

    def get_os_install_disk_options(self):
        """List mount options for partition devices of installed os"""
        return self.Get('os_disk_options')

    def get_os_install_disk_mount(self):
        """List mounted points for installed system"""
        rootdev = self.Get('os_install_root_dev')
        disk_hash = self.Get('os_disk_hash')
        fstab = FStab('/etc/fstab')
        fstabHasSwap = bool(fstab.isExists(what=FStab.TYPE,eq="swap"))
        def getMountPoint(disk):
            """Get mount point for disk. If fstab not contains swap then
            for swap use all swap formated partition"""
            if disk == rootdev:
                return "/"
            elif not fstabHasSwap and "format" in disk_hash[disk] and \
                "swap" in disk_hash[disk]['format']:
                return "swap"
            else:
                mount_point = fstab.getBy(eq=disk)
                if mount_point in self.nonTransferedDirs:
                    return ""
                else:
                    return mount_point
        return map(lambda x: getMountPoint(x),
               sorted(
               self.Get('os_disk_hash').keys(),key=self.separateDevice))

    def get_os_disk_mount(self):
        """List mounted points for current operation system"""
        disk_hash = self.Get('os_disk_hash')
        fstab = FStab('/etc/fstab')
        rootdev = self.Get('os_root_dev')
        return map(lambda x: '/' if x == rootdev else fstab.getBy(eq=x) or "",
                    sorted(self.Get('os_disk_hash').keys(),
                           key=self.separateDevice))

    def get_os_disk_format(self):
        """List filesystem for partition devices"""
        return self.getAttributeFromHash('os_disk_hash','format')

    def get_os_install_disk_format(self):
        """Install list filesystem for partition devices"""
        return self.Get('os_disk_format')

    def get_os_install_disk_grub(self):
        """List grub id for partition devices"""
        def swapfirstnum(x,first,second):
            disk,delimeter,part = x.partition(',')
            if disk == str(first):
                return "%d,%s"%(second,part)
            elif disk == str(second):
                return "%d,%s"%(first,part)
            return x

        curDiskGrub = self.getAttributeFromHash('os_disk_hash','grub')
        bootDev=sorted(
                filter(lambda x:x[0] in ('/','/boot'),
                zip(self.Get('os_install_disk_mount'),
                    self.Get('os_disk_grub'))),
                key=lambda x:x[0],reverse=True)
        if bootDev:
            bootDev=bootDev[0][1].partition(',')[0]
            bootDevName = filter(lambda x:str(x[1])==bootDev,
                          zip(self.Get('os_device_dev'),
                              self.Get('os_device_map')))
            if bootDevName:
                if bootDevName[0][0] == self.Get('os_install_mbr'):
                    return map(lambda x:swapfirstnum(x,bootDevName[0][1],0),
                           curDiskGrub)
        return curDiskGrub

    def get_os_disk_grub(self):
        """List grub id for partition devices"""
        return self.getAttributeFromHash('os_disk_hash','grub')

    def get_os_disk_part(self):
        """Type of partition devices (primary, extended or logical)"""
        return self.getAttributeFromHash('os_disk_hash','part')

    def get_os_disk_table(self):
        """Partition table for this partition"""
        return self.getAttributeFromHash('os_disk_hash','table')

    def get_os_install_disk_perform_format(self):
        """Needformat partitions"""
        return self.getAttributeFromHash('os_disk_hash','needformat')

    def get_os_disk_size(self):
        """Partition size"""
        return self.getAttributeFromHash('os_disk_hash','size')

    def get_os_disk_type(self):
        """Partition type"""
        return self.getAttributeFromHash('os_disk_hash','type')

    def get_os_disk_content(self):
        """Partition content"""
        distrRep = DistributiveRepository()
        osProberHash = getOsProberHash(getContentFunc=distrRep._getfromcontent)
        devicesHash = self.Get('os_device_hash')
        def detectContent(devName,devType,devParent,mountPoint):
            if mountPoint == '/':
               return "{short}-{march}{build}".format(
                       short=self.Get('os_linux_shortname'),
                       march=self.Get('os_arch_machine'),
                       build="-%s"%(self.Get('os_linux_build') if
                             self.Get('os_linux_build') else
                             self.Get('os_linux_ver')))
            content = osProberHash.get(devName,'')
            if not content and ((devParent in devicesHash and \
                devicesHash[devParent]['type'] == "flash") or 
                "cdrom" in devType or devType.endswith('raid') or
                devType.endswith('lvm')):
                dataInfo = distrRep._getfromcontent(devName)
                if "build" in dataInfo and dataInfo['build']:
                    if "cdrom" in devType:
                        dataInfo['livetype'] = " livecd"
                    elif devType.endswith('raid') or devType.endswith('lvm'):
                        dataInfo['livetype'] = ""
                    else:
                        dataInfo['livetype'] = " liveusb"
                    content = \
                        "{name}-{march}-{build}{livetype}".format(
                            **dataInfo)
            return content
        return map(lambda x:detectContent(x[0],x[1],x[2],x[3]),
               zip(self.Get('os_disk_dev'),
                   self.Get('os_disk_type'),
                   self.getAttributeFromHash('os_disk_hash','parent'),
                   self.Get('os_disk_mount')))

    def get_os_disk_name(self):
        """Label of partitions"""
        return self.getAttributeFromHash('os_disk_hash','name')

    def get_os_device_dev(self):
        """Devices"""
        return sorted(self.Get('os_device_hash').keys(),
                      key=self.separateDevice)

    def get_os_device_type(self):
        """Device type (hdd,cdrom,usb-flash)"""
        return self.getAttributeFromHash('os_device_hash','type')

    def get_os_device_map(self):
        """Map number for grub"""
        return self.getAttributeFromHash('os_device_hash','map')

    def get_os_device_table(self):
        """Table on device"""
        return self.getAttributeFromHash('os_device_hash','table')

    def get_os_install_grub_devicemap_conf(self):
        """Content of device.map file for grub"""
        rootType = self.Get('os_install_root_type')
        return "\n".join(map(lambda x: "(hd%s)   %s" % (x[0],x[1]),
                                filter(lambda x:x[2] == rootType,
                                zip(self.Get('os_device_map'),
                                    self.Get('os_device_dev'),
                                    self.Get('os_device_type')))))

    def get_os_install_fstab_mount_conf(self):
        """Information about mount points for fstab"""
        devicesForFstab = sorted(filter(lambda x:x[1] != "" and x[1] != "swap",
                                        zip(self.Get('os_install_disk_use'),
                                        self.Get('os_install_disk_mount'),
                                        self.Get('os_install_disk_format'),
                                        self.Get('os_install_disk_options'),
                                        self.Get('os_disk_dev'))),
                                 lambda x,y: cmp(self.separateDevice(x[1]),
                                                 self.separateDevice(y[1])))
        if self.Get('os_install_scratch') == "on":
            devicesForFstab = filter(lambda x:x[1] != "/", devicesForFstab)

        # rootLine one string, but it correct work if devicesForFstab is empty
        rootLine =  "\n".join(map(lambda x: "%s\t%s\t%s\t%s\t0 2" %
                                (self._comment(x[0],x[1],x[4]),x[1],x[2],x[3]),
                                        devicesForFstab[:1]))
        otherLines = "\n".join(map(lambda x: "%s\t%s\t%s\t%s\t0 0" %
                                (self._comment(x[0],x[1],x[4]),x[1],x[2],x[3]),
                                        devicesForFstab[1:]))

        bindData = zip(self.Get('os_install_bind_path'),
                       self.Get('os_install_bind_mountpoint'))

        bindLines = "\n".join(map(lambda x: "%s\t%s\tnone\tbind\t0 0"\
                                  %(x[0],x[1]), bindData))
        return "\n".join(filter(lambda x: x, [rootLine,otherLines,bindLines]))



    #def get_os_net_config_info(self):
        #"""Content of /etc/conf.d/net file"""

        #def getNetGateways():
            #"""Get the network gateways"""
            #execStr = 'route -n'
            #res = self._runos(execStr,env={"LANG":"C"})
            #if res is False:
                #cl_overriding.printERROR("Failed to execute '%s'"%execStr)
                #cl_overriding.exit(1)
            #flagData = False
            ## retData = {'eth0':'10.0.0.1' ...}
            #retData = {}
            #for line in res:
                #if flagData:
                    #routeData = map(lambda y: y.strip(),
                                    #filter(lambda x: x, line.split(" ")))
                    #if len(routeData) == 8:
                        #if routeData[3] == "UG" and routeData[7]!="lo":
                            #retData[routeData[7]] = routeData[1]
                #if line.startswith('Destination'):
                    #flagData = True
            #return retData

        #strIpAllow = self.Get('os_net_ip')
        #strNetAllow = self.Get('os_net_allow')
        #strInterfInfo = self.Get('os_net_interfaces_info')
        #systemName = self.Get('os_linux_shortname')
        #ret = ""
        #retList = []
        #gatewaysData = getNetGateways()
        #if strIpAllow and strNetAllow:
            #listDevInfo = map(lambda x: x.split(" "),strInterfInfo.split(","))
            #listDevInfo = map(lambda x: (x[0], x[1][1:-1]),listDevInfo)
            #listDevInfo = filter(lambda x: x[1]!="Off" ,listDevInfo)
            #listIpNet = map(lambda x: (x[0],"%s/%s"%(x[1],x[2])),
                            #zip(listDevInfo,strIpAllow.split(","),
                                #map(lambda x: x.partition("/")[2],
                                    #strNetAllow.split(","))))
            #flagNotPlug = not systemName in ("CLS")
            #for info, ipNet in listIpNet:
                #dev, data = info 
                #if data=="DHCP":
                    #retList.append('config_%s=( "dhcp" )'%dev)
                    #if flagNotPlug:
                        #retList.append('modules_%s=( "!plug" )'%dev)
                #else:
                    #retList.append('config_%s=( %s )'%(dev,ipNet))
                    #if gatewaysData and dev in gatewaysData:
                        #retList.append('routes_%s=( "default gw %s" )'\
                                        #%(dev,gatewaysData[dev]))
                    #if flagNotPlug:
                        #retList.append('modules_%s=( "!plug" )'%dev)
        #return "\n".join(retList)

    def _comment(self,s,mp,dev):
            if s.startswith("UUID"):
                return "# %s was on %s during installation\n%s" % (mp,dev,s)
            else:
                return s

    def get_os_install_fstab_swap_conf(self):
        """Information about swap for fstab"""
        return "\n".join(map(lambda x: "%s\tnone\tswap\tsw\t0 0"%\
                                    self._comment(x[0],"swap",x[2]),
                                filter(lambda x: x[1] == "swap",
                                    zip(self.Get('os_install_disk_use'),
                                        self.Get('os_install_disk_mount'),
                                        self.Get('os_disk_dev')))))

    def get_os_install_linux_system(self):
        """Get linux system (server or desktop)"""
        shortName = self.Get('os_install_linux_shortname')
        return self.dictNameSystem.get(shortName,"")

    def get_os_install_kernel_scheduler(self):
        """Get scheduler for install root device"""
        root_dev = filter(lambda x: x[1] == '/',
                            zip(self.Get('os_disk_dev'),
                                self.Get('os_install_disk_mount')))
        if root_dev:
            root_dev = root_dev[0][0]
            root_dev = filter(lambda x: x in root_dev,
                              self.Get('os_device_dev'))
            if root_dev:
                root_dev = root_dev[0].rpartition('/')[2]
                pathScheduler = '/sys/block/%s/queue/scheduler'%root_dev
                if access(pathScheduler,R_OK):
                    res = re.search("\[([^\]]+)\]",
                                    open(pathScheduler).read(),re.S)
                    if res:
                        return res.groups()[0]
        return "cfq"

    def get_os_kernel_schedulers(self):
        
        """Get scheduler for install root device"""
        root_dev = self.Get('os_device_dev')
        if root_dev:
            root_dev = root_dev[0].rpartition('/')[2]
            pathScheduler = '/sys/block/%s/queue/scheduler'%root_dev
            return map(lambda x:x.strip("[]"),
                   readFile(pathScheduler).strip().split())
        return []

    def get_os_install_lvm_set(self):
        """Using lvm"""
        lvm = filter(lambda x:x[1] != '' and "lvm" in x[0],
                    zip(self.Get('os_disk_type'),
                        self.Get('os_install_disk_mount')))
        return "on" if lvm else "off"

    def get_os_install_mdadm_set(self):
        """Using mdadm"""
        mdadm = filter(lambda x:x[1] != '' and "raid" in x[0],
                    zip(self.Get('os_disk_type'),
                        self.Get('os_install_disk_mount')))
        return "on" if mdadm else "off"

    def get_os_install_kernel_attr(self):
        """Kernel attributes"""
        # on usb-hdd install must be "delay=5"
        attr = ""
        if self.Get('os_install_root_type') == 'usb-hdd':
            attr = " scandelay=5"
        if self.Get('os_install_mdadm_set') == 'on':
            attr += " domdadm"
        if self.Get('os_install_lvm_set') == 'on':
            attr += " dolvm"
        return attr

    def get_os_install_kernel_resume(self):
        """install kernel resume parameter"""
        for dev, install in zip(self.Get('os_install_disk_use'),
                                self.Get('os_install_disk_mount')):
            if install == "swap":
                return "real_resume=%s"%dev
        return ""

    def get_os_install_arch_machine(self):
        """install maching architecture"""
        return self.Get('os_arch_machine')

    def get_cl_image_path(self):
        if self.Get('os_root_type') == "livecd":
            if self.Get('os_scratch') == "on" and path.exists('/mnt/flash'):
                livedistr = ['/mnt/flash']
            elif path.exists('/mnt/squash'):
                livedistr = ['/mnt/livecd']
            else:
                livedistr = ['/mnt/cdrom']
        else:
            livedistr = []
        rootDev = self.Get('os_install_root_dev')
        livedistr += \
            map(lambda x:x[0],
            filter(lambda x:" live" in x[1] and x[0] != rootDev,
            zip(self.Get('os_disk_dev'),
                self.Get('os_disk_content'))))
        return ['/var/calculate/remote/linux',
                '/var/calculate/linux'] + livedistr

    def getImage(self,scratch,rootType,imagePath,archMachine,
                 shortName,linuxVer=None):
        """Get image by parameters"""
        if scratch == "on" or rootType == "flash":
            discardType = ["dir"]
        else:
            discardType = []
        if type(imagePath) == str:
            imagePath = [imagePath]
        distRep = DistributiveRepository(imagePath)
        return distRep.getBestDistributive(march=archMachine,
                                           shortname=shortName.lower(),
                                           discardType=discardType,
                                           version=linuxVer) or ""

    def get_cl_image(self):
        """Get image file from distributive repository"""
        if self.Get('cl_action') != 'system':
            return ""
        return self.getImage(self.Get('os_install_scratch'),
                             self.Get('os_install_root_type'),
                             self.Get('cl_image_path'),
                             self.Get('os_install_arch_machine'),
                             self.Get('os_install_linux_shortname'),
                             None)
                             #self.Get('os_install_linux_ver'))

    def get_os_install_linux_shortname(self):
        """Shortname of installation os"""
        return self.Get('os_linux_shortname')

    def calculateini_to_dict(self,filename):
        if os.access(filename,os.R_OK):
            return dict(map(lambda x: x.strip().rpartition('=')[0::2],
                            open(filename,'r')))
        else:
            return {}

    def get_os_install_linux_ver(self):
        """Linux version of installation os"""
        if self.Get('cl_action') != "system":
            return self.Get('os_linux_ver')
        imagename = self.getImage(self.Get('os_install_scratch'),
                                  self.Get('os_install_root_type'),
                                  self.Get('cl_image_path'),
                                  self.Get('os_install_arch_machine'),
                                  self.Get('os_install_linux_shortname'))
        if not imagename and self.Get('cl_action') != 'merge':
            return ""
        d = DistributiveRepository()._getfromcontent(imagename)
        if "linuxver" in d and d['linuxver'] != "0":
            return d['linuxver']
        elif "ver" in d and d['ver'] != "0":
            return d['ver']
        res = DistributiveRepository.reDistName.search(imagename)
        if res:
            return res.groupdict()['ver']
        if self.Get('os_install_linux_shortname') == \
            self.Get('os_linux_shortname'):
            return self.Get('os_linux_ver')
        return "0"

    def get_cl_pkgdir_path(self):
        return "/var/calculate/remote/packages/%s/%s" % (
                self.Get('os_linux_shortname'),
                self.Get('os_install_arch_machine'))

    def get_os_install_linux_subname(self):
        """Subname of installation os"""
        linuxShortName = self.Get("os_install_linux_shortname")
        return self.dictLinuxSubName.get(linuxShortName,"")

    def get_os_install_linux_name(self):
        """Name of installation os"""
        linuxShortName = self.Get("os_install_linux_shortname")
        return self.dictLinuxName.get(linuxShortName,"Linux")

    def get_os_bind_hash(self):
        """List mounted points for current operation system"""
        # convert fstab to
        # [['/dev/sda3', '/', '', 'reiserfs', 'noatime', '', '', '0', '2\n'],
        # ['/dev/sda5', '/var/calculate', 'reiserfs', 'noatime', '0', '0\n']]
        listFstab = filter(lambda x: len(x)>=4,
                    map(lambda x: filter(lambda x: x,
                                         x.replace('\t',' ').split(' ')),
                        filter(lambda x: not x.startswith('#') and x.strip(),
                               open("/etc/fstab"))))
        return dict(map(lambda x:[x[0],x[1]],
                        filter(lambda x: "bind" in x[3],
                               listFstab)))

    def get_os_bind_path(self):
        """Directories for bind"""
        return sorted(self.Get('os_bind_hash').keys())

    def get_os_bind_mountpoint(self):
        """Mountpoint for directories bind"""
        bindhash = self.Get('os_bind_hash')
        return [ bindhash[i] for i in sorted(bindhash.keys()) ]

    def get_os_install_bind_path(self):
        """Install directories for bind"""
        return map(lambda x:x[0],
                   # skip nonTransferedDirs
                   filter(lambda x: not x[0] in self.nonTransferedDirs and
                                    not x[1] in self.nonTransferedDirs,
                          zip(self.Get('os_bind_path'),
                              self.Get('os_bind_mountpoint'))))

    def get_os_install_bind_mountpoint(self):
        """Mountpoint for install directories bind"""
        return map(lambda x:x[1],
                   # skip nonTransferedDirs
                   filter(lambda x: not x[0] in self.nonTransferedDirs and
                                    not x[1] in self.nonTransferedDirs,
                          zip(self.Get('os_bind_path'),
                              self.Get('os_bind_mountpoint'))))

    def get_os_install_root_dev(self):
        return self.Get('os_root_dev')

    def get_os_install_clock_timezone(self):
        """timezone for clock"""
        return self.Get('os_clock_timezone')

    def get_os_install_clock_type(self):
        """type of clock (UTC or local)"""
        clockTypeFile = ['/etc/conf.d/clock','/etc/conf.d/hwclock']
        for f in clockTypeFile:
            clock = self.getValueFromConfig(f,"clock")
            if clock:
                if clock.upper() == 'UTC':
                    return clock.upper()
                elif clock.lower() == 'local':
                    return clock.lower()
        return "local"
    
    def get_os_install_x11_resolution(self):
        """Xorg resolution"""
        # get resolution from xorg.log
        res = self.Get('os_x11_resolution')
        if res or self.Get('os_install_root_type') in ('livecd','usb-hdd'):
            return res
        else:
            return "1024x768"
    
    def get_os_install_x11_video_drv(self):
        """Video driver used by xorg"""
        if self.Get('os_install_root_type') == 'usb-hdd':
            prefix = self.Get('cl_chroot_path')
            videoVal = self.getValueFromCmdLine("calculate","video")
            videoVal = {'i915':'intel'}.get(videoVal,videoVal)
            if not isPkgInstalled('xorg-server',
                                  prefix=self.Get('cl_chroot_path')) or \
                videoVal in getAvailableVideo(prefix=prefix):
                return videoVal
            return self.getVideoByDefault(getInstalledVideo(prefix=prefix))
        else:
            return self.Get('os_x11_video_drv')
    
    def get_os_install_x11_composite(self):
        """On/off composite"""
        defaultCompositeOn = ("nvidia","intel","fglrx","nouveau","radeon")
        composite = self.getValueFromCmdLine("calculate",5)
        videodrv = self.getValueFromCmdLine("calculate",4)
        if videodrv != "auto":
            composite = {'nocomposite':'off',
                         'off':'off',
                         'on':'on',
                         'composite':'on'}.get(composite)
        else:
            composite = None

        if self.Get('os_x11_video_drv') in defaultCompositeOn:
            defaultComposite = "on"
        else:
            defaultComposite = "off"
        state = self.get_composite_from_xorgconf()
        return composite or state or defaultComposite

    def get_os_install_linguas(self):
        """Current linguas"""
        def get_linguas(lines):
            linguas = map(lambda x:x.strip().rpartition('=')[-1].strip('"\''),
                       filter(lambda x: x.startswith("LINGUAS="),
                              lines))
            return linguas[-1] if linguas else ""
        makeconf = '/etc/make.conf'
        infocommand = 'emerge --info'
        # get linguas from make.conf, emerge --info or default
        curlanguage = self.Get('os_install_locale_language')
        return get_linguas(open(makeconf,'r')) or \
               " ".join(filter(lambda x:x=="en" or x==curlanguage,
                      get_linguas(self._runos(infocommand) or []).split())) or \
               "bg en de es fr it pl pt_BR ru uk"

    def get_os_install_locale_consolefont(self):
        """consolefont"""
        locale = clLocale()
        return locale.getFieldByKeymap("consolefont",
                            self.Get('os_install_locale_keymap'))

    def get_os_install_locale_keymap(self):
        """keymap of locale (used for /etc/conf.d/keymaps)"""
        locale = clLocale()
        # get keymap from boot calculate param (keymap specified
        # by lang)
        keymapConfd = '/etc/conf.d/keymaps'
        keymap = self.getValueFromCmdLine("calculate",1)
        if locale.isLangExists(keymap):
            return locale.getFieldByLang('keymap',keymap)
        # get keymap by os_install_locale_lang
        keymap = self.getValueFromConfig(keymapConfd,'KEYMAP')
        if keymap:
            return keymap
        return locale.getFieldByLang("keymap",
                        self.Get("os_install_locale_lang"))

    def get_os_install_locale_dumpkeys(self):
        """dumpkeys charset for keymap"""
        locale = clLocale()
        # is specified keymap support by locale hash
        if self.Get('os_install_locale_keymap') in locale.getFields('keymap'):
            return locale.getFieldByKeymap("dumpkeys_charset",
                                        self.Get('os_install_locale_keymap'))
        else:
            return locale.getFieldByLang("dumpkeys_charset",
                                        self.Get('os_install_locale_lang'))

    def get_os_install_locale_locale(self):
        """locale (example: ru_RU.UTF-8)"""
        return self.Get('os_locale_locale')

    def get_os_install_locale_lang(self):
        """lang (example: ru_RU)"""
        locale = clLocale()
        return locale.getLangByField("locale",
                    self.Get('os_install_locale_locale'))

    def get_os_install_locale_language(self):
        """language (example: ru)"""
        locale = clLocale()
        return locale.getFieldByLang("language",
                    self.Get('os_install_locale_lang'))

    def get_os_install_locale_xkb(self):
        """xkb layouts (example: en,ru)"""
        locale = clLocale()
        return locale.getFieldByLang("xkblayout",
                    self.Get('os_install_locale_lang'))

    def get_os_install_locale_xkbname(self):
        """названия используемых раскладок клавиатуры для X"""
        localeXkb = self.Get("os_install_locale_xkb")
        if localeXkb:
            return localeXkb.split("(")[0]
        return ""

    def get_os_install_net_hostname(self):
        """Computer hostname"""
        return self.Get("os_net_hostname") or "calculate"

    def get_os_install_net_allow(self):
        """Allowed network"""
        return self.Get("os_net_allow")

    def get_os_install_net_ip(self):
        """Current ip"""
        return self.getAttributeFromHash('os_install_net_hash','ip')

    def get_os_install_net_cidr(self):
        """Current mask"""
        return self.getAttributeFromHash('os_install_net_hash','cidr')

    def get_os_install_net_network(self):
        """Current network"""
        return self.getAttributeFromHash('os_install_net_hash','network')

    def get_os_install_net_mac(self):
        """Current mac"""
        return self.getAttributeFromHash('os_install_net_hash','mac')

    def get_os_install_net_name(self):
        """Current mac"""
        return self.getAttributeFromHash('os_install_net_hash','name')

    def get_os_install_net_gateway(self):
        """Get default gateway"""
        ipCmd = checkUtils("/sbin/ip")
        reDefault = re.compile("^default via ({0})".format(ip.IP_ADDR))
        gw = map(lambda x:x.group(1),
            filter(lambda x:x,
            map(reDefault.search,
            process(ipCmd,"route"))))
        if gw:
            return gw[0]
        else:
            return ""

    def isDNSByDHCP(self):
        """If first interface get ip by DHCP dns must be DHCP"""
        dhcps = self.Get('os_install_net_dhcp_set')
        if dhcps:
            if dhcps[0] == "on":
                return True
        return False

    def get_os_install_net_dns(self):
        """Get current name servers"""
        dnsIps = filter(ip.checkIp,
                 map(lambda x:x.strip().partition("nameserver")[2].strip(),
                 filter(lambda x:x.lstrip().startswith("nameserver"),
                 readLinesFile('/etc/resolv.conf'))))
        return "" if self.isDNSByDHCP() else " ".join(dnsIps)

    def get_os_install_net_dns_search(self):
        """Get current name servers"""
        dnsSearch = " ".join(
                    map(lambda x:x.strip().partition("search")[2].strip(),
                    filter(lambda x:x.lstrip().startswith("search"),
                    readLinesFile('/etc/resolv.conf'))))
        return "" if self.isDNSByDHCP() else dnsSearch

    def get_os_install_net_conf(self):
        """Net setup (networkmanager or openrc)"""
        if filter(lambda x:x.lower() == ("networkmanager"),
           listDirectory('/etc/runlevels/boot')+
           listDirectory('/etc/runlevels/default')) \
            or self.Get('os_install_root_type') == "livecd" and \
               self.Get('os_install_root_dev') != "/dev/nfs":
            if isPkgInstalled("net-misc/networkmanager",
                              prefix=self.Get('cl_chroot_path')):
                return "networkmanager"
        return "openrc"

    def get_os_install_net_interfaces(self):
        """Net interfaces"""
        return sorted(self.Get('os_install_net_hash').keys())

    def get_os_install_net_domain(self):
        """Domain"""
        if path.exists('/proc/self/fd/1') and \
            readlink('/proc/self/fd/1') == '/dev/console' and \
            self.Get('os_root_dev') == '/dev/nfs':
            return "local"
        textLines = self._runos("hostname -d 2>&1")
        domain = ""
        if textLines:
            domain = textLines[0]
        return domain or "local"

    def get_os_install_root_type(self):
        """Type of device for install"""
        rootdev = self.Get('os_install_root_dev')
        devicetype = getPartitionDevice(
                        getUdevDeviceInfo(name=rootdev).get('DEVPATH',''))
        devicetype = map(lambda x: x[1],
                     filter(lambda x:x[0] == devicetype,
                     zip(self.Get('os_device_dev'),
                         self.Get('os_device_type'))))
        if devicetype:
            return devicetype[0]
        else:
            return self.Get('os_root_type')

    def get_os_install_dev_from(self):
        """Detect previous system"""
        calculate1ini = "/etc/calculate/calculate.ini"
        calculate1param = "install.devfrom"
        if path.exists(calculate1ini):
            res = reduce(lambda x,y: [y.rpartition('=')[2].strip()],
                      filter(lambda x: x.partition("=")[0] == calculate1param,
                             open(calculate1ini,"r")),
                      "")
            if res:
                return res[-1]
        return ""

    def get_os_install_makeopts(self):
        """Get makeopts for make.conf file"""
        cpunum = self.Get('hr_cpu_num')
        if cpunum == 1:
            return "-j1"
        else:
            return "-j%d"%(cpunum+1)

    def get_os_install_kernel(self):
        bootdir = path.join(self.Get('cl_chroot_path'),'boot')
        modulesdir = path.join(self.Get('cl_chroot_path'),'lib/modules')
        validKernel = listDirectory(modulesdir)
        kernelFiles = self.getFilesByType(bootdir,"Linux kernel")
        installMarch = self.Get('os_install_arch_machine')
        kernelsWithVer = \
            map(lambda x:(x[0],(getTupleVersion("".join(x[1].groups()[0:3:2])),
                                path.getmtime(x[0]))),
                filter(lambda x:x[1] and x[1].group() in validKernel and
                       installMarch in x[0].rpartition('/')[2],
                       map(lambda x:(x[0],self.reFindVer.search(x[1])),
                       kernelFiles)))
        if kernelsWithVer:
            return path.split(max(kernelsWithVer,key=itemgetter(1))[0])[-1]
        else:
            return "vmlinuz"

    def get_os_install_initrd(self):
        return self.getInitrd(self.Get('os_install_arch_machine'),
                              self.Get('os_install_linux_shortname'),
                              self.Get('cl_chroot_path'),
                              self.Get('os_install_kernel'),
                              suffix="",notsuffix="-install") or \
               self.getInitrd(self.Get('os_install_arch_machine'),
                              self.Get('os_install_linux_shortname'),
                              self.Get('cl_chroot_path'),
                              self.Get('os_install_kernel'),
                              suffix="-install")[:-8] \
                              or "initrd"

    def get_os_install_initrd_install(self):
        return self.getInitrd(self.Get('os_install_arch_machine'),
                              self.Get('os_install_linux_shortname'),
                              self.Get('cl_chroot_path'),
                              self.Get('os_install_kernel'),
                              suffix="-install") or "initrd-install"

    def get_os_install_kernel_config(self):
        configfile = self.Get('os_install_kernel').replace('vmlinuz', 'config')
        if configfile.startswith('config') and path.exists(
                path.join(self.Get('cl_chroot_path'),'boot',configfile)):
            return configfile
        else:
            return ""

    def get_os_install_system_map(self):
        systemmapfile = self.Get('os_install_kernel').replace('vmlinuz',
                        'System.map')
        if systemmapfile.startswith('System.map') and path.exists(
                path.join(self.Get('cl_chroot_path'),'boot',systemmapfile)):
            return systemmapfile
        else:
            return ""

    def get_os_grub_conf(self):
        """Use for generate grub.conf for new system"""
        pathGrubConf = "/boot/grub/grub.conf"
        replace = ""
        if os.access(pathGrubConf,os.R_OK):
            reRemoveComments = re.compile("(^|\n)[\t ]*#[^\n]*?(?=\n|$)", re.S)
            reGrubEntry = re.compile("(title.*?)(?=title|$)", re.S | re.I )
            reRootEntry = re.compile("root=(\S+)")
            grubconf = reRemoveComments.sub("",open(pathGrubConf,'r').read())
            roothd = filter(lambda x: x[1] == '/',
                            zip(self.Get('os_disk_dev'),
                                self.Get('os_install_disk_mount'),
                                self.Get('os_disk_uuid')))
            if roothd:
                transRoot = \
                    filter(lambda x:x != roothd[0][0] and \
                                    x != "UUID=%s"%roothd[0][2],
                    self.Get('os_disk_dev')+
                        map(lambda x:"UUID=%s"%x,
                        filter(lambda x:x,self.Get('os_disk_uuid'))))
                return ("".join(
                    map(lambda x:x[0],
                    filter(lambda x:not x[1] or x[1].groups()[0] in transRoot,
                    map(lambda x:(x,reRootEntry.search(x)),
                    reGrubEntry.findall(grubconf)))))).strip()
        return ""

    def get_cl_migrate_user(self):
        """migrate users"""
        return getPasswdUsers()

    def get_cl_migrate_user_pwd(self):
        retList = []
        fileName = "/etc/shadow"
        if access(fileName, R_OK):
            migrateusers = self.Get("cl_migrate_user")
            if migrateusers:
                lenData=9
                shadowData = filter(lambda x: len(x)==lenData,
                                    map(lambda x: x.rstrip().split(":"),
                                    open(fileName)))
                shadowData = filter(lambda x: x[0] in migrateusers, shadowData)
                shadowData = map(lambda x: (x[0], x[1]), shadowData)
                shadowUsers = map(lambda x: x[0], shadowData)
                for userName in migrateusers:
                    if userName in ("root"):
                        if userName in shadowUsers:
                            userData = filter(lambda x: x[0]==userName,
                                              shadowData)
                            hashPwd = userData[0][1]
                            salt = "".join(hashPwd.rpartition("$")[:1])
                            if salt and crypt(userName, salt) == hashPwd:
                                retList.append("yes")
                            else:
                                retList.append("no")
                        else:
                            retList.append("yes")
                    else:
                        if userName in shadowUsers:
                            retList.append("no")
                        else:
                            retList.append("yes")
        return retList

    def get_os_format_type(self):
        """Filesystem format support by calcualte-install"""
        return filter(lambda x:not x in ("default","noformat") and
                               x in convertDictOpt._fileSystemOpt,
               filter(lambda x:not x.startswith("_"),
                      convertDictOpt._fileSystemOpt.keys())+\
                      convertDictOpt._propertiesFileSystem.keys())

    def get_os_format_use(self):
        """Avialable format by mkfs utility"""
        return map(lambda x:"yes" 
            if path.exists(convertDictOpt._fileSystemOpt[x]["makefs"])
            else "no", self.Get('os_format_type'))

    def get_os_install_scratch(self):
        """Install system in scratch mode"""
        return self.Get('os_scratch')

    def getFieldByField(self,resField,field,value, firstPrefix="os_disk",
                        secondPrefix="os_disk"):
        """Get value of field specified by 'firstPrefix_resField',
        by field specified by 'secondPrefix_resField==value'"""
        res = filter(lambda x: x[1] == value,
                zip(self.clVars.Get('%s_%s'%(firstPrefix,resField)),
                    self.clVars.Get('%s_%s'%(secondPrefix,field)))) or\
                    [("","")]
        return res[0][0]

    def get_os_install_mbr(self):
        if self.Get('os_install_pxe') == 'on':
            return ''
        if self.Get('os_install_root_type') in ("flash","usb-hdd"):
            rootdev = self.Get('os_install_root_dev')
            device = filter(lambda x:x in rootdev,
                                self.Get('os_device_dev'))
            if device:
                return device[0]
            else:
                return ""
        # if loaded system livecd
        if self.Get('os_root_type') == "livecd":
            # search /boot device or / device, by priority /boot,/
            bootDev=sorted(
                    filter(lambda x:x[0] in ('/','/boot'),
                    zip(self.Get('os_install_disk_mount'),
                        self.Get('os_disk_dev'))),
                    key=lambda x:x[0],reverse=True)
            if bootDev:
                bootdev = bootDev[0][1]
                # search device for partition
                device = filter(lambda x:x in bootdev,
                                    self.Get('os_device_dev'))
                # set it by default
                if device:
                    return device[0]
        if self.Get('os_device_dev'):
            return self.Get('os_device_dev')[0]
        return ""

    def get_cl_install_autoupdate_set(self):
        if self.Get('ac_install_system') == "up":
            if self.Get('os_install_linux_system') == 'desktop':
                return "on"
            else:
                return "off"
        else:
            return self.Get('cl_autoupdate_set')

    def get_cl_install_kernel_uid(self):
        return self.getKernelUid(self.Get('os_install_root_dev'))

    def get_os_nvidia_mask(self):
        nvidiaeclass = pathJoin(self.Get('cl_chroot_path'),
                                'usr/portage/eclass/nvidia-driver.eclass')
        if not os.access(nvidiaeclass,os.R_OK):
            return ""
        category = "0300"
        vendor = "10de"
        lsPciProg = self.getProgPath("/usr/sbin/lspci")
        nvidiacards =  filter(lambda x:" %s: "%category in x,
                      self._runos('%s -d %s: -n'%(lsPciProg,vendor)))
        if not nvidiaeclass:
            return ""
        cardsid = map(lambda x:x.groups()[0],
             filter(lambda x:x,
                map(lambda x:re.search("[0-9a-fA-F]{4}:([0-9a-fA-F]{4})",x),
                     nvidiacards)))
        if not cardsid:
            return ""
        eclassdata = open(nvidiaeclass,'r').read()
        drv_categories = re.findall('^drv_([^=]+)="', eclassdata, re.M)
        drvs = map(lambda x:(x[0],x[1].replace('\\\n','').split()),
                    re.findall('\ndrv_(%s)="(.*?)"'%"|".join(drv_categories),
                               eclassdata,re.S))
        mask_categories = re.findall('^mask_([^=]+)="', eclassdata, re.M)
        masks = dict(map(lambda x:(x[0],x[1].replace('\\\n','')),
                    re.findall('\nmask_(%s)="(.*?)"'%"|".join(drv_categories),
                               eclassdata,re.S)))
        drvsForCardsid = filter(lambda x:set(x[1])&set(cardsid),drvs)
        if drvsForCardsid and drvsForCardsid[0][0] in masks:
            return masks[drvsForCardsid[0][0]]
        return ""

    def get_os_install_linux_build(self):
        """Linux build"""
        imagename = self.getImage(self.Get('os_install_scratch'),
                                  self.Get('os_install_root_type'),
                                  self.Get('cl_image_path'),
                                  self.Get('os_install_arch_machine'),
                                  self.Get('os_install_linux_shortname'))
        if imagename in ('/mnt/flash', '/mnt/squash',
                         '/mnt/livecd', '/mnt/cdrom'):
            return self.Get('os_linux_build')
        if imagename:
            d = DistributiveRepository()._getfromcontent(imagename)
            if d and "build" in d:
                build = d['build']
                return build
            res = DistributiveRepository.reDistName.search(imagename)
            if res:
                build = res.groupdict()['ver']
                if re.match('^\d{8}$',build):
                    return build
        return ""

    def get_os_install_fb_resolution(self):
        """Get current framebuffer resolution"""
        textLines = self._runos("/sbin/fbres 2>&1")
        resolution = ""
        if textLines:
            cxx11,cyx11 = \
                self.Get('os_install_x11_resolution').partition('x')[0::2]
            cxfb, cyfb = textLines[0].partition('x')[0::2]
            if not filter(lambda x:not x.isdigit(),[cxfb,cyfb,cxx11,cyx11])and \
                int(cxx11) >= int(cxfb) and int(cyx11) >= int(cyfb):
                resolution = "%s-32"%textLines[0]
        return resolution or "1024x768-32@60"

    def get_os_grub2_path(self):
        """Get Grub2 Install cmd"""
        # find grub2-install
        grubInstall = self.getProgPath('/sbin/grub2-install')
        if grubInstall:
            return grubInstall
        # find grub-install and check, that this is grub2-install (ver 1.99)
        grubInstall = self.getProgPath('/sbin/grub-install')
        if grubInstall and filter(lambda x:"1.99" in x,
                           process(grubInstall,'-v')):
            return grubInstall
        return ""

    def get_cl_chroot_grub(self):
        """Chroot for grub-mkconfig"""
        if self.Get('os_install_scratch') == "on":
            return path.join(self.Get('cl_chroot_path'),"mnt/scratch")
        else:
            return self.Get('cl_chroot_path')

    def get_cl_autologin(self):
        """Autologin"""
        if self.Get('os_install_root_type') == "livecd" or \
            self.Get('os_install_linux_shortname') == "CMC":
            nonRootUsers = filter(lambda x: x != "root",
                           self.Get('cl_migrate_user'))
            if nonRootUsers:
                return nonRootUsers[0]
        else:
            return ""

    def get_os_install_kernel_cpufreq(self):
        """Get cpufreq (and other from modules_3= param) from conf.d/modules"""
        cpufreqmods = map(lambda x:x.partition('=')[2].strip("\n '\""),
                      filter(lambda x:x.startswith('modules_3'),
                      readLinesFile('/etc/conf.d/modules')))
        if cpufreqmods:
            return cpufreqmods[0]
        else:
            return ""
