#-*- coding: utf-8 -*-

# Copyright 2010 Calculate Ltd. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import os, sys, re

from cl_apply_template import updateConfFiles, __app__, __version__
from cl_opt import opt
from cl_share_cmd import share_cmd
from cl_lang import lang
from cl_utils import listDirectory,pathJoin
from os import path
lang().setLanguage(sys.modules[__name__])

USAGE = _("%prog [options] path_to_clt_file")

COMMENT_EXAMPLES = _("Apply all templates to all packages")

EXAMPLES = _("%prog --locate all")

DESCRIPTION = _("Calculate Linux templates utility")

CMD_OPTIONS = [{'shortOption':"l",
                'longOption':"locate",
                'optVal':"LOCATE",
                'type':'choice',
                'choices':['all','clt','local','remote'],
                'help':_("select location templates \
'all','clt','local','remote'")},
               {'longOption':'merge',
                'optVal':"PACKAGE",
                'help':_("apply templates to specified package only")
               },
               {'longOption':"clt",
                'optVal':"CLT_TEMPLATE",
                'help':_("process the clt template (by default)")
               }]

class apply_template_cmd(share_cmd):
    def __init__(self):
        # Объект опций командной строки
        self.optobj = opt(\
            package=__app__,
            version=__version__,
            usage=USAGE,
            examples=EXAMPLES,
            comment_examples=COMMENT_EXAMPLES,
            description=DESCRIPTION,
            option_list=CMD_OPTIONS + opt.color_control + opt.variable_control,
            check_values=self.checkOpts)
        # Создаем объект логики
        self.logicObj = updateConfFiles()

    reVerSplit = re.compile(r"^(.*?)-(([^-]+?)(?:-(r\d+))?)(?:.(tbz2))?$",re.S)

    def checkAtom(self,atom):
        """Chech if atom is installed"""
        dbPkg = '/var/db/pkg'
        if "/" in atom:
            category,slash,package = atom.partition("/")
            categoryPath = pathJoin(dbPkg,category)
            return \
                  map(lambda x:"%s/%s"%(category,x.groups()[0]),
                  filter(lambda x:x and x.groups()[0] == package,
                  map(self.reVerSplit.search, 
                  filter(lambda x:x.startswith(package),
                  listDirectory(categoryPath)))))
        else:
            return reduce(lambda x,y:x+self.checkAtom("%s/%s"%(y,atom)),
                   listDirectory(dbPkg),[])

    def checkOpts(self, optObj, args):
        """Проверка опций командной строки"""
        if optObj.merge:
            if not self.checkAtom(optObj.merge):
                errMsg = _("incorrect option")+":"+\
                         " --%s" %"merge" + \
                         ": "+_("wrong package") + ": " +\
                         str(optObj.merge)
                self.optobj.error(errMsg)
                return False

        if len(args) == 0:
            if not optObj.clt and not optObj.l:
                errMsg = _("option required")+":  --clt "+_("or")+" --locate"
                self.optobj.error(errMsg)
                return False
        elif len(args) == 1:
            if optObj.clt:
                errMsg = _("incorrect option")+":"+" --%s" %"clt" + " " + \
                           str(optObj.clt)
                self.optobj.error(errMsg)
                return False
            if optObj.l:
                errMsg = _("incorrect option")+":"+" --%s" %"locate" + " " + \
                           str(optObj.l)
                self.optobj.error(errMsg)
                return False
        else:
            errMsg = _("incorrect argument") + ":" + " %s" %" ".join(args)
            self.optobj.error(errMsg)
            return False
        self.optobj.checkVarSyntax(optObj)
        return optObj, args

    def applyCltTemplate(self, cltTemplate):
        """Применяем clt шаблон"""
        if cltTemplate[0] != "/":
            currentPath = os.getcwd()
            cltTemplate = os.path.join(currentPath, cltTemplate)
        return self.logicObj.applyCltTemplate(cltTemplate)

    def applyAllTemplates(self,pkg):
        """Применяем все шаблоны"""
        return self.logicObj.applyAllTemplates(package=pkg)

    def applyLocalTemplates(self,pkg):
        """We use local templates"""
        return self.logicObj.applyAllTemplates(location="local",package=pkg)
    
    def applyRemoteTemplates(self,pkg):
        """We use remote templates"""
        return self.logicObj.applyAllTemplates(location="remote",package=pkg)
    
    def applyCltTemplates(self):
        """We use all clt templates"""
        return self.logicObj.applyAllTemplates(location="clt")
