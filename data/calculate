#!/sbin/runscript
# Copyright 2011 Calculate Ltd. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

depend() {
	need localmount root fsck modules
	before keymaps consolefont dbus
	use swap hostname
}

check_install_ability()
{
	local pkg=$1
	FEATURES="-getbinpkg" FETCHCOMMAND=false DISTDIR=/usr/portage/distfiles emerge -fq $pkg &>/dev/null
}

is_already_install()
{
	local pkg=$1
	local category=`echo $pkg | cut -d\/ -f1`
	local pkg=`echo $pkg | cut -d\/ -f2`
	local categoryPath=/var/db/pkg/${category}
	[[ -e $categoryPath ]] && ls $categoryPath | grep -q "^$pkg"
}

install_pkg()
{
	local pkg=$1
	ebegin "Installing $pkg"
	FEATURES="-getbinpkg" FETCHCOMMAND=false DISTDIR=/usr/portage/distfiles emerge $pkg &>/dev/null
}

variable_value()
{
	local varname=$1
	cl-install -vv --filter $varname | 
		sed -rn "s/^$varname\s+\[[rw]\]\s+(.*)$/\1/p"
}

install_video_driver()
{
	ebegin "Checking video drivers"
	case `variable_value os_install_x11_video_drv` in
		fglrx)
			local drv="x11-drivers/ati-drivers"
			;;
		nvidia)
			local nvidiamask=`variable_value os_nvidia_mask`
			[[ -n $nvidiamask ]] && \
				echo $nvidiamask >/etc/portage/package.mask/nvidia
			local drv="x11-drivers/nvidia-drivers"
			;;
		*)
			true
			;;
	esac
	if [[ -n $drv ]]
	then
		if ! is_already_install $drv
		then
			check_install_ability $drv && install_pkg $drv
		fi
		eend $?
	else
		eend 0
	fi
}

# create user guest if system has not non-root user
create_nonroot_user() {
	if which cl-desktop &>/dev/null &&
	  [[ -z $(variable_value cl_migrate_user | sed -r "s/[][,']|root//g") ]] &&
		[[  `variable_value os_install_linux_system` != "server" ]]
	then
		# passwd of non root user
		local nonroot_pw='$5$e/joOLXL$5GfArWgJx7BEcGqil3hTZoPGqWLRUUjCbzvl3gTWN75'
		# name of  non root user
		local nonroot_user="guest"
		ebegin "Creating ${nonroot_user} user"
		useradd -p "${nonroot_pw}" \
			--groups users,wheel,audio,cdrom,video,cdrw,usb,plugdev,games,lp,scanner,uucp \
			${nonroot_user}
		eend $?
	fi
}

# create/update all user profile
configure_users() {
	if which cl-desktop &>/dev/null
	then
		for user in $(variable_value cl_migrate_user | sed -r "s/[][,']//g")
		do
			HOME_DIR=`getent passwd $user | awk -F: '{ print( $6 ); }'`
			if [[ $user == "root" ]] || [[ -f $HOME_DIR/.calculate/ini.env ]]
			then
				cl-desktop $user
			fi
		done
	fi
}

start() {
	install_video_driver
	LANG=C create_nonroot_user
	LANG=C /usr/bin/cl-install --startup --live
	LANG=C configure_users

	/usr/sbin/env-update
	source /etc/profile
	/sbin/update-modules

	# reinit net.* services
	local roottype=`variable_value os_root_type`
	[[ $roottype == "livecd" ]] && udevadm trigger --action="add" --subsystem-match=net

	/sbin/rc-update --update
	return 0
}

stop() {
	local roottype=`variable_value os_root_type`
	if [[ $roottype == "hdd" ]] && rc-config list boot | grep -q calculate
	then
		rc-update del calculate boot
		local distdir="/usr/portage/distfiles"
		[[ -d "${distdir}" ]] && [[ -n "`ls ${distdir}`" ]] && rm ${distdir}/*
	fi
}
