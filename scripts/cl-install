#!/usr/bin/env python2
#-*- coding: utf-8 -*-

# Copyright 2010 Calculate Ltd. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import sys
import os
sys.path.insert(0,os.path.abspath('/usr/lib/calculate-2.2/calculate-lib/pym'))
sys.path.insert(0,
                os.path.abspath('/usr/lib/calculate-2.2/calculate-install/pym'))

from cl_lang import lang
tr = lang()
tr.setGlobalDomain('cl_install')
tr.setLanguage(sys.modules[__name__])
from cl_install_cmd import install_cmd


if __name__ == "__main__":
    install = install_cmd()
    install.logicObj.initVars()
    # set lang 
    ret = install.optobj.parse_args()
    if ret is False:
        sys.exit(1)
    options, args = ret
    # set color/nocolor for display messages
    install.setPrintNoColor(options)
    # init variables
    install.setProxyNtpHostname(options.proxy,options.ntp,options.hostname)
    if not install.setNetworkParams(options.ip,options.route,options.dns,
                            options.netconf,options.dhcp):
        sys.exit(1)
    if options.l:
        if not install.setLang(options.l):
            sys.exit(1)
    # set values to variables
    if not install.setVars(options):
        sys.exit(1)
    # check and set installed options
    install.setAction(options.startup,options.live)
    if not (options.startup or options.install or options.uninstall or
            options.live):
        if not install.checkAndSetInstallOptions(options.d,options.w,
                                                 options.u,options.A):
            sys.exit(1)
    # print variables
    if options.v or options.filter or options.xml:
        install.printVars(options)
        sys.exit(0)
    if options.p:
        install.showPartitions()
        sys.exit(0)
    # check root 
    if not install.isRoot():
        sys.exit(1)
    # configurate current system
    if options.startup:
        if not install.configureSystem(options.A):
            sys.exit(1)
    elif options.install:
        if not install.installPackage():
            sys.exit(1)
    elif options.uninstall:
        if not install.uninstallPackage():
            sys.exit(1)
    else:
        forceOpions = options.f or options.P
        flagSpinner = not options.nospinner
        if not install.installSystem(force=forceOpions, bootDisk=options.mbr,
                          stdinReadPwd=options.P,builder=options.build,
                          flagSpinner=flagSpinner,update=options.U,
                          pxe=options.pxe):
            sys.exit(1)
    #if not install.writeVars(options):
    #    sys.exit(1)
    sys.exit(0)
